use std::{
	collections::HashMap,
	io::{Read, Write},
	net::{TcpListener, TcpStream},
	str::Lines,
};

fn main() {
	println!("NDT is arrived!");

	let Ok(listener) = TcpListener::bind("127.0.0.1:4221") else {
		panic!("unable to listen 127.0.0.1:4221")
	};

	for stream in listener.incoming() {
		let Ok(s) = stream else { return };
		println!("A connection arrived: {:?}", s);
		let req = process_request(s);
		println!("{m} {p} {v}", m = req.method, p = req.path, v = req.version);
		println!("{:?}", req.headers);
	}
}

fn process_request(mut stream: TcpStream) -> Request {
	let mut buf = vec![0; 2048];
	stream.read(&mut buf).unwrap();
	let req = Request::from_buffer(buf);

	let _ = stream.write(match req.path.as_str() {
		"/" => b"HTTP/1.1 200 OK\r\n\r\nHi there",
		_ => b"HTTP/1.1 404 Not Found\r\n\r\nWrong Number",
	});

	req
}

struct Request {
	method: String,
	path: String,
	version: String,
	headers: HashMap<String, String>,
}

impl Request {
	fn from_buffer(vec: Vec<u8>) -> Self {
		let Ok(req) = String::from_utf8(vec) else {
			panic!("Unable to parse connection request!")
		};

		let mut lines = req.lines();

		let Some(startline) = lines.next() else {
			panic!("Empty request!")
		};

		let (method, path, version) = Self::parse_startline(startline);
		let headers = Self::parse_headers(lines);

		Self {
			method,
			path,
			version,
			headers,
		}
	}

	fn parse_startline(raw: &str) -> (String, String, String) {
		let mut segements = raw.split_whitespace();
		println!("{:?}", segements);

		let Some(method) = segements.next() else {
			panic!("Unable to get request method");
		};
		let Some(path) = segements.next() else {
			panic!("Unable to get request path");
		};
		let version = match segements.next() {
			Some(result) => result,
			None => "any",
		};

		(method.to_string(), path.to_string(), version.to_string())
	}

	fn parse_headers(lines: Lines) -> HashMap<String, String> {
		let mut headers = HashMap::new();
		for line in lines {
			let parts: Vec<_> = line.splitn(2, ':').collect();
			if parts.len() < 2 {
				continue;
			}
			headers.insert(parts[0].to_string(), parts[1].to_string());
		}
		headers
	}
}
